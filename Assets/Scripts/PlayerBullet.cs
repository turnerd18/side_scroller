﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour {

    public float lifeTime = 5.0f;
    public float speed = 10.0f;
    public Vector3 direction = Vector3.zero;

    private float createTime;
    private Transform player;

	// Use this for initialization
	void Start () {
        createTime = Time.time;
        player = GameObject.Find("Barrel").transform;
	}
	
	// Update is called once per frame
	void Update () {
        if (createTime + lifeTime < Time.time)
            Destroy(gameObject);

        transform.Translate(direction.normalized * speed * Time.deltaTime, player);
        transform.Rotate(new Vector3(0, 20.0f , 0));
	}
}
