﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public float lifeTime = 5.0f;
    public float speed = 10.0f;
    public Vector3 direction = Vector3.zero;

    private float createTime;

	// Use this for initialization
	void Start () {
        createTime = Time.time;    
	}
	
	// Update is called once per frame
	void Update () {
        if (createTime + lifeTime < Time.time)
            Destroy(gameObject);

        transform.Translate(direction.normalized * speed * Time.deltaTime, transform);
	}
}
