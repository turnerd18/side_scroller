﻿using UnityEngine;
using System.Collections;

public class EnemyTurret : MonoBehaviour {

    public Transform bullet;
    public Transform barrelLeft;
    public Transform barrelRight;
    public Transform explosion;

    private Transform target;
    private int bulletsFired = 0;
    private float lastShot = 0;
    private float fireRate = 1.0f;

	// Use this for initialization
	void Start () {
        target = GameObject.FindGameObjectWithTag("Target").transform;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 dir = target.position - transform.position;
        if (dir.magnitude < 10.0f)
        {
            transform.rotation = Quaternion.LookRotation(dir, Vector3.up);

            if (lastShot + fireRate < Time.time)
            {
                Transform barrel = (bulletsFired % 2) == 0 ? barrelLeft : barrelRight;
                Transform bulletTransform = Instantiate(bullet, barrel.position, barrel.rotation) as Transform;
                Bullet newBullet = bulletTransform.GetComponent("Bullet") as Bullet;
                newBullet.speed = 20.0f;
                newBullet.direction = target.position;

                lastShot = Time.time;
                bulletsFired++;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerBullet")
        {
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
