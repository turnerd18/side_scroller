﻿using UnityEngine;
using System.Collections;

public class SSPlayer : MonoBehaviour {

    public int fallCount;
    public Transform projectile;
    public Transform barrel;

    public int hitPoints = 10;
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y < -5.0f)
        {
            transform.position = new Vector3(transform.position.x, 1.5f, transform.position.z);
            fallCount++;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Transform bulletTransform = Instantiate(projectile, barrel.position, barrel.rotation) as Transform;
            bulletTransform.eulerAngles = new Vector3(0, 0, 90);
            PlayerBullet bullet = bulletTransform.GetComponent("PlayerBullet") as PlayerBullet;
            bullet.speed = 20.0f;

            Vector3 mousePos = Input.mousePosition;
            Vector3 toPos = Camera.main.ScreenToWorldPoint(mousePos);
            toPos.x = 0;
            toPos.z -= transform.position.z;
            toPos.z += 2.5f; // offset the running
            Debug.Log(toPos);
            bullet.direction = toPos;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if ((other.tag == "Bullet") && (hitPoints > 0))
            hitPoints--;
    }
}
