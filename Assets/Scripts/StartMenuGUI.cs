﻿using UnityEngine;
using System.Collections;

public class StartMenuGUI : MonoBehaviour {

    public float buttonWidth, buttonHeight;
    public string fileType, fileExtension;
    public GUISkin guiSkin;

    protected string m_textPath;

    protected FileBrowser m_fileBrowser;

    [SerializeField]
    protected Texture2D m_directoryImage,
                        m_fileImage;

    private AudioSource sound;

    void Start()
    {
        sound = GameObject.Find("Sound").audio;
    }

    protected void OnGUI()
    {
        GUI.skin = guiSkin;
        if (m_fileBrowser != null)
        {
            m_fileBrowser.OnGUI();
        }
        else
        {
            OnGUIMain();
        }
    }

    protected void OnGUIMain()
    {

        GUILayout.BeginHorizontal();
        GUILayout.Label(fileType + " Music File:", GUILayout.Width(100));
        GUILayout.FlexibleSpace();
        GUILayout.Label(m_textPath ?? "none selected");
        if (GUILayout.Button("...", GUILayout.ExpandWidth(false)))
        {
            m_fileBrowser = new FileBrowser(
                new Rect(100, 100, 600, 500),
                "Choose Text File",
                FileSelectedCallback
            );
            m_fileBrowser.SelectionPattern = "*." + fileExtension;
            m_fileBrowser.DirectoryImage = m_directoryImage;
            m_fileBrowser.FileImage = m_fileImage;
        }
        GUILayout.EndHorizontal();
        if( m_textPath != null )
        {
            if (GUI.Button(new Rect((Screen.width / 2) - (buttonWidth / 2), (Screen.height / 2) - buttonHeight, buttonWidth, buttonHeight), "Test Music") && sound.clip)
            {
                sound.Play();
            }
        }
        if (GUI.Button(new Rect((Screen.width / 2) - (buttonWidth / 2), (Screen.height / 2) - buttonHeight + 40, buttonWidth, buttonHeight), "Play Game"))
        {
            DontDestroyOnLoad(sound.gameObject);
            Application.LoadLevel("level");
        }
    }

    protected void FileSelectedCallback(string path)
    {
        m_fileBrowser = null;
        m_textPath = path;
        if (m_textPath != null)
        {
            WWW www = new WWW("file://" + m_textPath);
            sound.Stop();
            sound.clip = www.audioClip;
        }
    }
}
