﻿using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour {

    private SSPlayer player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent("SSPlayer") as SSPlayer;
    }

	// Use this for initialization
	void OnGUI () {
        GUI.Label(new Rect((Screen.width / 2) - 50, 75, 100, 20), "Fallen " + player.fallCount + " times");
	}
}
