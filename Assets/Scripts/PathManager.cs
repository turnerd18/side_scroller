﻿using UnityEngine;
using System.Collections;

public class PathManager : MonoBehaviour
{

    public float nextInsertZ = 88.0f;
    public float pathSpacing = 44.0f;
    public Transform[] paths;

    private int pathsAdded = 0;
    private Transform player;
    private Queue pathQ;
    private bool firstUpdate = true;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        pathQ = new Queue();
    }

    // Update is called once per frame
    void Update()
    {
        if (firstUpdate)
        {
            pathQ.Enqueue(GameObject.Find("Path 1"));
            pathQ.Enqueue(GameObject.Find("Path 2"));
            firstUpdate = false;
        }

        //Debug.Log(player.position.z + " " + pathSpacing+ " " + pathsAdded);
        if ((player.position.z / pathSpacing) > pathsAdded)
        {

            pathsAdded++;
            int index = (int)Random.Range(0, paths.Length);
            Transform newPath = Instantiate(paths[index]) as Transform;
            pathQ.Enqueue(newPath.gameObject);
            newPath.position = new Vector3(0, 0, nextInsertZ);
            
            nextInsertZ += pathSpacing;

            if (pathsAdded > 2)
            {
                GameObject oldPath = pathQ.Dequeue() as GameObject;
                Destroy(oldPath);
            }
        }
    }
}