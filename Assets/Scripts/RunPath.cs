﻿using UnityEngine;
using System.Collections;

public class RunPath : MonoBehaviour {

    public float nextInsertZ = 88.0f;
    public float pathSpacing = 44.0f;
    public Transform[] paths;

    private int pathsAdded = 0;
    private Transform player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
    void Update()
    {
        //Debug.Log(player.position.z + " " + pathSpacing+ " " + pathsAdded);
        if ((player.position.z / pathSpacing) > pathsAdded)
        {

            pathsAdded++;
            Debug.Log((int)Random.Range(0, paths.Length));
            Transform newPath = Instantiate(paths[1]) as Transform;
            newPath.position = new Vector3(0, -4.5f, nextInsertZ-44);
            newPath.parent = transform;

            nextInsertZ += pathSpacing;
            Transform[] t = GetComponentsInChildren<Transform>();
            if (pathsAdded > 1)
            {
                Destroy(t[1].gameObject);
            }
        }
	}
}
