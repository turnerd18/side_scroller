﻿using UnityEngine;
using System.Collections;

public class SSCamera : MonoBehaviour {

    public float camRelativeHeight = 1.5f;
    public float camRelativeDistance = 10.0f;
	
	// Update is called once per frame
	void Update () {
        Camera.main.transform.position = transform.position + new Vector3(camRelativeDistance, camRelativeHeight, 0);
	}
}
